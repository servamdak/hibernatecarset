package com.carSet.service;

import com.carSet.dao.CarDao;
import com.carSet.daoImplementation.CarDaoImpl;
import com.carSet.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Collection;
import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarDao carDao;

    public Car findCarById(int id) {
        return carDao.findCarById(id);
    }

    public void addCar(Car car) {
        carDao.addCar(car);
    }

    public Collection<Car> getAllCars() {
        return carDao.getAllCars();
    }

    public void updateCarParameters(Car car) {
        carDao.updateCarParameters(car);
    }

    public void deleteCar(Car car) {
        carDao.deleteCar(car);
    }
}
