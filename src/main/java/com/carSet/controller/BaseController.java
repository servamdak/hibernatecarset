package com.carSet.controller;

import com.carSet.entity.Car;
import com.carSet.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class BaseController {

    @Autowired
    private CarService carService;

    @GetMapping(path = "/carbase", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Car returnData(){


        return carService.findCarById(2);
    }
}
