package com.carSet;

import com.carSet.entity.Car;
import com.carSet.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import sun.tools.jar.CommandLine;

@SpringBootApplication()
public class App{
//public class App implements CommandLineRunner{

    @Autowired
    static CarService carService;

    @Autowired
    Car car;


    public static void main(String[] args) {
        SpringApplication.run(App.class, args);

    }


//    @Override
//    public void run(String... strings) throws Exception {
//
//        System.out.println(carService.findCarById(2));
//    }
}
