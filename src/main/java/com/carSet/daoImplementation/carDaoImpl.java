package com.carSet.daoImplementation;

import com.carSet.configuration.HibernateUtility;
import com.carSet.dao.CarDao;
import com.carSet.entity.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Component
public class CarDaoImpl implements CarDao {


//    private Session session;

//    @Autowired
//    private SessionFactory sessionFactory;


//    public CarDaoImpl(Session session) {
//        session = HibernateUtility.currentSession();
//    }

//    Session session = HibernateUtility.currentSession();

    @Transactional
    public Car findCarById(int id) {
        Session session = HibernateUtility.getSessionFactory().openSession();
//        Session session = HibernateUtility.currentSession();
        return session.load(Car.class, id);
    }

    @Transactional
    public void addCar(Car car) {
        Session session = HibernateUtility.getSessionFactory().openSession();
//        Session session = HibernateUtility.currentSession();
        session.save(car);
    }

    @Transactional
    public List<Car> getAllCars() {
        Session session = HibernateUtility.getSessionFactory().openSession();
//        Session session = HibernateUtility.currentSession();
        CriteriaQuery<Car> criteriaQuery = session.getCriteriaBuilder().createQuery(Car.class);
        criteriaQuery.from(Car.class);
        List<Car> cars = session.createQuery(criteriaQuery).getResultList();
        return cars;
    }

    @Transactional
    public void updateCarParameters(Car car) {
        Session session = HibernateUtility.getSessionFactory().openSession();
//        Session session = HibernateUtility.currentSession();
        session.update(car);
    }

    @Transactional
    public void deleteCar(Car car) {
        Session session = HibernateUtility.getSessionFactory().openSession();
//        Session session = HibernateUtility.currentSession();
        session.delete(car);
    }


}
