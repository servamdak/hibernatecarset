package com.carSet.configuration;


import com.carSet.entity.Car;
import com.carSet.service.CarService;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class AppConfiguration {

    @Value("${database.driver}")
    public String PROPERTY_DRIVER;

    @Value("${database.url}")
    public String PROPERTY_URL;

    @Value("${database.user}")
    public String PROPERTY_USERNAME;

    @Value("${database.password}")
    public String PROPERTY_PASSWORD;

    @Value("${hibernate.showSql}")
    public String PROPERTY_SHOW_SQL;


    @Value("${hibernate.dialect}")
    public String PROPERTY_DIALECT;


    @Value("${hibernate.hbm2ddl.auto}")
    public String PROPERTY_HBM2DDL;

    //Test
//    @Value("${database.createSchema}")
    public String PROPERTY_SCHEMA;

//    @Value("${hibernate.current_session_context_class}")
//    public String PROPERTY_CURRENT_SESSION_CONTEXT_CLASS;


    //Test
//    @Bean
//    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
//        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
//        ppc.setLocation(new ClassPathResource("application.properties"));
//        ppc.setIgnoreUnresolvablePlaceholders(true);
//        return ppc;
//    }

    //Test
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public DriverManagerDataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(PROPERTY_DRIVER);
        dataSource.setUrl(PROPERTY_URL);
        dataSource.setUsername(PROPERTY_USERNAME);
        dataSource.setPassword(PROPERTY_PASSWORD);
        //Test
//        dataSource.setSchema(PROPERTY_SCHEMA);
        return dataSource;
    }

    @Bean
    public Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", PROPERTY_DIALECT);
        properties.setProperty("hibernate.hbm2ddl.auto", PROPERTY_HBM2DDL);
        properties.setProperty("hibernate.showSql", PROPERTY_SHOW_SQL);
//        properties.setProperty("hibernate.current_session_context_class", PROPERTY_CURRENT_SESSION_CONTEXT_CLASS);
        return properties;
    }

    @Bean
    public SessionFactory sessionFactory() {
        return new LocalSessionFactoryBuilder(dataSource())
                .scanPackages("com.carSet")
                .addProperties(hibernateProperties())
                .buildSessionFactory();
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
        hibernateTransactionManager.setSessionFactory(sessionFactory);
        return hibernateTransactionManager;
    }
//
//    @Bean
//    public CarService carService(){
//        return new CarService();
//    }

    @Bean
    public Car car(){
        return new Car();
    }

}
