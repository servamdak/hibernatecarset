package com.carSet.configuration;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;


public class HibernateUtility {


    private static SessionFactory sessionFactory;

    static {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable e) {
            System.err.println("Initial SessionFactory creation failed. " + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static final ThreadLocal session = new ThreadLocal();

    public static Session currentSession() throws HibernateException {
        Session newSession = (Session) session.get();

        if (newSession == null) {
            newSession = sessionFactory.openSession();
            session.set(newSession);
        }
        return newSession;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
