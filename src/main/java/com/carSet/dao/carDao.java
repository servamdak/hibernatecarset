package com.carSet.dao;

import com.carSet.entity.Car;

import java.util.Collection;


public interface CarDao {

    Car findCarById(int id);

    void addCar(Car car);

    Collection<Car> getAllCars();

    void updateCarParameters(Car car);

    void deleteCar(Car car);

}
