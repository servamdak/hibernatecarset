DROP TABLE IF EXISTS CAR;

CREATE TABLE CAR (ID BIGINT NOT NULL AUTO_INCREMENT, NAME VARCHAR(32), YEAR INT, COLOR VARCHAR(32), FUELTYPE VARCHAR(32), CONSTRAINT PK_CAR PRIMARY KEY (ID));

INSERT INTO CAR VALUES (1, 'ford', 1992, 'black', 'diesel');
INSERT INTO CAR VALUES (2, 'toyota', 2010, 'yellow', 'petrol');
INSERT INTO CAR VALUES (3, 'honda', 2013, 'black', 'petrol');
INSERT INTO CAR VALUES (4, 'mercedes', 2017, 'blue', 'petrol');
INSERT INTO CAR VALUES (5, 'toyota', 2003, 'white', 'diesel');
INSERT INTO CAR VALUES (6, 'bmw', 2018, 'red', 'diesel');
INSERT INTO CAR VALUES (7, 'kia', 1999, 'green', 'petrol');